package com.groundgurus.sbfileuploadexample.controller;

import com.groundgurus.sbfileuploadexample.service.StorageService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileUploadController {
  private StorageService storageService;

  public FileUploadController(StorageService storageService) {
    this.storageService = storageService;
  }

  @PostMapping("/upload")
  public String handleFileUpload(@RequestParam MultipartFile file) {
    storageService.store(file);

    return "redirect:/";
  }
}
